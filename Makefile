EMACS_DIR:=~/.emacs.d

SRC:=init.el customize.el
DIR:=backups site-config

install:
	cp -r ${SRC} ${DIR} ${EMACS_DIR}
