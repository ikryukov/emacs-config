;; This thrives to be a simple but usable emacs config

;; 0. Prelims

;; Use MELPA 
(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
(package-initialize)

;; Install use-package
(unless (require 'use-package nil :noerror)
  (package-refresh-contents)
  (package-install 'use-package))
(require 'use-package)

(setq use-package-always-ensure t
      use-package-compute-statistics t)

;; 1. Settings

;; Custom file
(setq custom-file (expand-file-name "customize.el" user-emacs-directory))
(load custom-file)

;; Backup folder
(defvar emacs-backup-directory
  (concat user-emacs-directory "backups/"))
(setq backup-directory-alist `((".*" . ,emacs-backup-directory))
      auto-save-file-name-transforms `((".*" ,emacs-backup-directory t)))

;; GUI
(tool-bar-mode 0)
(scroll-bar-mode 0)
(recentf-mode 1)
(tooltip-mode 0)
(show-paren-mode 1)
(setq show-paren-delay 0)
(global-visual-line-mode 1)
(defalias 'yes-or-no-p 'y-or-n-p)

(setq inhibit-startup-screen t
      initial-scratch-message nil
      ring-bell-function 'ignore
      sentence-end-double-space nil
      recentf-max-saved-items 100)

(setq-default c-basic-offset 4
	      fill-column 100)

;; 2. Packages

;; Evil
(use-package evil
  :init 
  (setq evil-want-C-u-scroll t)
  :config (evil-mode 1))

;; Ivy-Counsel
(use-package counsel
  :init
  (ivy-mode 1)
  (counsel-mode 1)
  :bind (("C-x C-b" . ivy-switch-buffer)
	 ("C-x f" . counsel-recentf)
	 ("C-c y" . counsel-yank-pop)))

;; Org
(use-package ik-org
  :load-path "site-config"
  :after org
  :config
  (setq org-startup-indented t
	org-latex-pdf-process
	'("pdflatex -interaction nonstopmode -output-directory %o --synctex=1 %f"
	  "bibtex %b"
	  "makeindex %b"
	  "pdflatex -interaction nonstopmode -output-directory %o --synctex=1 %f"
	  "pdflatex -interaction nonstopmode -output-directory %o --synctex=1 %f")))

;; which-key
(use-package which-key
  :config (which-key-mode 1))

;; dired-sidebar
(use-package dired-sidebar
  :commands (dired-sidebar-toggle-sidebar)
  :bind (("C-c \\" . dired-sidebar-toggle-sidebar)))

;; Editing
(use-package smartparens
  :hook (prog-mode . turn-on-smartparens-strict-mode)
  :init (require 'smartparens-config))

(use-package evil-smartparens
  :hook (smartparens-enabled . evil-smartparens-mode))

(use-package evil-surround
  :config (global-evil-surround-mode 1))

;; 3. Bindings
; Use emacs mode instead of evil insert
(evil-define-key 'emacs 'global (kbd "<escape>") 'evil-normal-state)
(defalias 'evil-insert-state 'evil-emacs-state)
(setq evil-emacs-state-cursor '(bar . 2))

(evil-define-key '(normal visual) 'global
		  "j" 'evil-next-visual-line
		  "J" 'evil-next-visual-line
		  "k" 'evil-previous-visual-line
		  "K" 'evil-previous-visual-line
		  "H" 'evil-beginning-of-visual-line
		  "L" 'evil-end-of-visual-line)
(evil-ex-define-cmd "q" 'kill-this-buffer)
(evil-ex-define-cmd "wq" '(lambda ()
			    (interactive)
			    (save-buffer)
			    (kill-this-buffer)))

;; 4. Language-specific stuff
(use-package ik-python
  :load-path "site-config"
  :after python
  :init
  (setq python-shell-interpreter "python3"))


(use-package snakemake-mode
  :bind (:map snakemake-mode-map
	      ("C-c c" . snakemake-popup)))

(use-package org-ref
  :after ik-org
  :config
  (setq org-ref-completion-library 'org-ref-ivy-cite
	reftex-default-bibliography '("~/Documents/bibliography/references.bib")
	org-ref-bibliography-notes "~/Documents/bibliography/notes.org"
	org-ref-default-bibliography '("~/Documents/bibliography/references.bib")
	org-ref-pdf-directory "~/Documents/bibliography/bibtex-pdfs/"))

;; 5. Environment

(setq exec-path (append exec-path '("~/.local/bin")))

;; 6. Shell
(use-package ik-eshell
  :load-path "site-config"
  :after eshell)
